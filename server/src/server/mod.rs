use axum::{extract::State, response::IntoResponse, routing::get, Router};
use core::{future::Future, ops::Deref};
use db::Db;
use hyper::{header, http, Body, StatusCode};
use prometheus::{Registry, TextEncoder};
use std::{net::SocketAddr, sync::Arc};

mod auth;
pub(crate) mod chat;
mod moderators;

pub async fn run<S, F, R>(
    registry: R,
    addr: S,
    db: Arc<Db>,
    moderators_token: Option<String>,
    shutdown: F,
) -> Result<(), hyper::Error>
where
    S: Into<SocketAddr>,
    F: Future<Output = ()>,
    R: Deref<Target = Registry> + Clone + Send + Sync + 'static,
{
    let tokens = Arc::new(auth::Tokens::new().unwrap());

    let metrics = Router::new()
        .route("/", get(metrics))
        .with_state(registry.deref().clone());

    let app = Router::new()
        .nest("/metrics", metrics)
        .nest(
            "/chat/v1",
            chat::router(Arc::clone(&db), Arc::clone(&tokens)),
        )
        .nest(
            "/moderators/v1",
            moderators::router(Arc::clone(&db), moderators_token),
        )
        .nest("/auth/v1", auth::router(db, tokens))
        .route("/health", get(|| async {}));

    // run it
    let addr = addr.into();
    let server =
        axum::Server::bind(&addr).serve(app.into_make_service_with_connect_info::<SocketAddr>());
    let server = server.with_graceful_shutdown(shutdown);
    tracing::info!("listening on {}", addr);
    match server.await {
        Ok(_) => tracing::debug!("webserver shutdown successful"),
        Err(e) => tracing::error!(?e, "webserver shutdown error"),
    }

    Ok(())
}

async fn metrics(State(registry): State<Registry>) -> Result<impl IntoResponse, StatusCode> {
    use prometheus::Encoder;
    let mf = registry.gather();
    let mut buffer = Vec::with_capacity(1024);

    let encoder = TextEncoder::new();
    encoder
        .encode(&mf, &mut buffer)
        .expect("write to vec cannot fail");

    match http::Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, "text/plain; charset=utf-8")
        .body(Body::from(buffer))
    {
        Err(e) => {
            tracing::warn!(?e, "could not export metrics to HTTP format");
            Err(StatusCode::INTERNAL_SERVER_ERROR)
        },
        Ok(r) => Ok(r),
    }
}
