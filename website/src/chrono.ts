/// Where is my chrono...
export function formatDate(date: Date): string {
    const zeroPad = (num: number) => String(num).padStart(2, '0')
    let year = String(date.getUTCFullYear());
    let month = zeroPad(date.getUTCMonth() + 1); // arrrrgg
    let day = zeroPad(date.getUTCDate());

    return [year, month, day].join('-');
}

export function formatTime(date: Date): string {
    const zeroPad = (num: number) => String(num).padStart(2, '0')
    let hours = zeroPad(date.getUTCHours());
    let min = zeroPad(date.getUTCMinutes());
    let sec = zeroPad(date.getUTCSeconds());

    return [hours, min, sec].join(':');
}

export function formatDateTime(datetime: Date, rust: boolean): string {
    let seperator = rust? 'T': ' '
    let endutc = rust? 'Z': ''
    let date = formatDate(datetime)
    let time = formatTime(datetime)
    return date + seperator + time + endutc;
}