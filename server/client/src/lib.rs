use api::v1;
use chrono::{DateTime, Utc};
use core::{future::Future, pin::Pin, time::Duration};
use reqwest::Client;
use reqwest::header::{HeaderMap, HeaderValue};

pub type DefaultCallbackFn =
    Box<dyn FnMut(Vec<v1::ChatMessage>) -> Pin<Box<dyn Future<Output = ()> + Send>> + Send>;

/// [api_password] is known as web_chat_secret in server-cli and specified here: https://gitlab.com/veloren/veloren/-/blob/7a0b3bff5bbd3e7396f656d9ba6d2174b16d751a/server-cli/src/settings.rs#L36
/// [gameserver_api_fqdn] is the base path of the api, e.g. `https://server.veloren.net` for the official gameserver
/// [latest_successful] specifies the time to start with, If you know previous
/// messages, e.g. from persistence, set to Some, otherwise None interval to
/// fetch.
/// [interval] specifies the fetch interval, most servers only cache 60s, so it
/// should be lower, recommended: 10s.
/// [callback] is executed for all fetched messages.
pub async fn run<S, F, Fut>(
    api_password: String,
    gameserver_api_fqdn: String,
    latest_successful: Option<DateTime<Utc>>,
    interval: Duration,
    callback: F,
    shutdown: S,
) where
    S: Future<Output = ()>,
    F: FnMut(Vec<v1::ChatMessage>) -> Fut,
    Fut: Future<Output = ()> + Send,
{
    let mut headers = HeaderMap::new();
    headers.insert(
        v1::X_SECRET_TOKEN,
        HeaderValue::from_str(&api_password).unwrap(),
    );

    let client = Client::builder()
        .user_agent("moderation-tool")
        .default_headers(headers)
        .build()
        .unwrap();

    match latest_successful {
        None => tracing::info!("Starting fetching with no Startdate, the database is empty"),
        Some(latest_successful) => {
            let secs = (Utc::now() - latest_successful).num_seconds();
            tracing::info!(?latest_successful, ?secs, "Starting fetching")
        },
    }

    tokio::select! {
        _ = shutdown => (),
        _ = async move {
            let mut latest_successful = latest_successful;
            let mut callback = callback;
            loop {
                latest_successful = step(&client, &gameserver_api_fqdn, &mut callback, latest_successful).await;
                tokio::time::sleep(interval).await;
                tracing::trace!("{interval:?} has elapsed, doing next fetch");
            }
        } => (),
    };
}

async fn step<F, Fut>(
    client: &Client,
    gameserver_fqdn: &str,
    callback: &mut F,
    latest_successful: Option<DateTime<Utc>>,
) -> Option<DateTime<Utc>>
where
    F: FnMut(Vec<v1::ChatMessage>) -> Fut,
    Fut: Future<Output = ()>,
{
    const HISTORY_ROUTE: &str = "/chat/v1/history";
    let mut request = client.get(format!("{gameserver_fqdn}{HISTORY_ROUTE}"));

    if let Some(date) = latest_successful {
        request = request.query(&[("from_time_exclusive_rfc3339", date.to_rfc3339())]);
    }

    let messages = match request.send().await {
        Ok(response) => match response.json::<Vec<v1::ChatMessage>>().await {
            Ok(messages) => messages,
            Err(e) => {
                tracing::error!(?e, "error parsing the gameserver api response");
                return latest_successful;
            },
        },
        Err(e) => {
            tracing::error!(?e, "error fetching the gameserver api");
            return latest_successful;
        },
    };
    let count = messages.len();
    if count > 0 {
        tracing::debug!(?count, "received messages");
    }
    let latest = messages.iter().max_by_key(|cm| cm.time).map(|cm| cm.time);
    callback(messages).await;

    latest.or(latest_successful)
}
