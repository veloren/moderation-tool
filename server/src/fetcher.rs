use api::v1;
use db::{actions, Db};
use std::sync::Arc;

async fn handle_messages(db: Arc<Db>, messages: Vec<v1::ChatMessage>) {
    for msg in messages {
        tracing::info!("{}", crate::api_ext::prettify_msg(&msg, true));
        if let Err(e) = actions::message::store(msg, &db).await {
            tracing::error!(?e, "error storing message");
        };
    }
}

pub fn store_messages(db: Arc<Db>) -> gameserver_api::DefaultCallbackFn {
    let db = db;
    Box::new(move |messages| {
        let db = Arc::clone(&db);
        Box::pin(handle_messages(db, messages))
    })
}
