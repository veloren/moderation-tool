export type Message = {
    time: string;
    parties: string;
    content: string;
};

export type PlayerName = {
    alias: string;
    uuid: string;
};

export function parseMessages(data: string): Array<Message> {
    var res: Array<Message> = JSON.parse(data);

    return res;
}

export function getType(msg: Message): string {
    let obj = JSON.parse(msg.parties)
    return Object.keys(obj)[0];
}

export function inspectParties(parties: string): {type: string, relatedPlayers: Array<PlayerName>} {
    let obj = JSON.parse(parties)
    let type = Object.keys(obj)[0];
    let relatedPlayers: Array<PlayerName> = Array<PlayerName>();
    let subobj =  Object.values(obj)[0];
    switch (type) {
        case "Online":
        case "Offline":
        case "CommandInfo":
        case "CommandError":
        case "Say":
        case "Faction":
        case "Region":
        case "World":
            relatedPlayers.push({
                alias: subobj["alias"],
                uuid: subobj["uuid"],
            })
            break;
        case "Kill": //Kill(KillSource, PlayerInfo),
            relatedPlayers.push({
                alias: subobj[1]["alias"],
                uuid: subobj[1]["uuid"],
            })
            let killtype = subobj[0];
            if (killtype == "Player") {
                let subsubobj =  Object.values(subobj[0])[0];
                relatedPlayers.push({
                    alias: subsubobj["alias"],
                    uuid: subsubobj["uuid"],
                })
            }
            break;
        case "Group": //Group(PlayerInfo, Vec<PlayerInfo>),
            relatedPlayers.push({
                alias: subobj[0]["alias"],
                uuid: subobj[0]["uuid"],
            })
            for (var subsubobj in subobj[1]) {
                relatedPlayers.push({
                    alias: subsubobj["alias"],
                    uuid: subsubobj["uuid"],
                })
            }
            break;
        case "Tell": //Tell(PlayerInfo, PlayerInfo),
            relatedPlayers.push({
                alias: subobj[0]["alias"],
                uuid: subobj[0]["uuid"],
            })
            relatedPlayers.push({
                alias: subobj[1]["alias"],
                uuid: subobj[1]["uuid"],
            })
            break;
    }


    let result = {
        type: type,
        relatedPlayers:  relatedPlayers,
    };
    return result;
}