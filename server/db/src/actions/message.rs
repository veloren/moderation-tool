use crate::{
    models::{simple, Message, RelatedPlayer},
    util, Db, DbType,
};
use api::v1::{ext::ChatPartiesExt, ChatMessage};
use chrono::{DateTime, Utc};
use sqlx::{
    any::{AnyArguments, AnyRow},
    Arguments, Executor, QueryBuilder, Row,
};
use tracing::instrument;

#[instrument(skip(db), err)]
pub async fn get_latest_timestamp(db: &Db) -> Result<Option<DateTime<Utc>>, sqlx::Error> {
    let row = sqlx::query("SELECT MAX(time) as time from message")
        .fetch_one(&db.pool)
        .await?;

    Ok(row
        .try_get("time")
        .ok()
        .and_then(|time| util::str_to_date(time, &db.t)))
}

#[instrument(skip(message, db), err)]
pub async fn store(message: ChatMessage, db: &Db) -> Result<i64, sqlx::Error> {
    let (message, mut related_players) = parse_message(message);
    let mut con = db.pool.begin().await?;
    let query = sqlx::query(
        "INSERT INTO message (time, party_type, parties, content) VALUES (?, ?, ?, ?) RETURNING ID",
    )
    .bind(message.time.to_string())
    .bind(message.party_type)
    .bind(message.parties)
    .bind(message.content);
    let row = con.fetch_one(query).await?;

    let id: i64 = row.try_get("id")?;
    // update the message_id of each related player entry
    for (player, _) in &mut related_players {
        player.message_id = id;
    }

    // insert related players
    let mut query_builder = QueryBuilder::new("INSERT INTO related_player (message_id, uuid)");
    query_builder.push_values(&related_players, |mut b, (player, _)| {
        b.push_bind(player.message_id)
            .push_bind(player.uuid.clone());
    });
    con.execute(query_builder.build()).await?;

    // try updating uuid aliases
    let mut query_builder = QueryBuilder::new("INSERT or IGNORE INTO alias (uuid, alias)");
    query_builder.push_values(related_players, |mut b, (player, alias)| {
        b.push_bind(player.uuid).push_bind(alias);
    });
    con.execute(query_builder.build()).await?;

    con.commit().await?;

    Ok(id)
}

#[instrument(skip(db), err)]
pub async fn history(
    from: DateTime<Utc>,
    to: DateTime<Utc>,
    db: &Db,
) -> Result<Vec<simple::Message>, sqlx::Error> {
    // Query for all message except for those sent directly or via group chats
    let rows = sqlx::query(
        r"SELECT id, CAST(time AS TEXT) as timeS, parties, content
              FROM message
              WHERE time >= ? AND time < ? AND party_type NOT IN (6,7)
              ORDER BY timeS ASC",
    )
    .bind(from.to_string())
    .bind(to.to_string())
    .fetch_all(&db.pool)
    .await?;

    rows.into_iter()
        .map(|row| parse_simple_message(row, &db.t))
        .collect::<Result<Vec<_>, _>>()
}

#[instrument(skip(db), err)]
pub async fn player_history(
    from: DateTime<Utc>,
    to: DateTime<Utc>,
    players_uuid: Vec<String>,
    db: &Db,
) -> Result<Vec<simple::Message>, sqlx::Error> {
    let mut args = AnyArguments::default();
    args.add(from.to_string());
    args.add(to.to_string());
    let mut query_builder = QueryBuilder::with_arguments(
        r"SELECT
            id,
            CAST(message.time AS TEXT) AS timeS,
            message.parties AS parties, 
            message.content AS content
        FROM message
        JOIN related_player
            ON message.id = related_player.message_id
        WHERE message.time >= ?
            AND message.time < ?
            AND (related_player.uuid) IN ",
        args,
    );
    query_builder.push_tuples(players_uuid, |mut b, uuid| {
        b.push_bind(uuid);
    });
    query_builder.push(" ORDER BY timeS ASC");
    let rows = query_builder.build().fetch_all(&db.pool).await?;

    rows.into_iter()
        .map(|row| parse_simple_message(row, &db.t))
        .collect::<Result<Vec<_>, _>>()
}

fn parse_message(message: ChatMessage) -> (Message, Vec<(RelatedPlayer, String)>) {
    let party_type = message.parties.party_type();
    // TODO: Handle these errors
    let parties = serde_json::to_string(&message.parties).unwrap();
    let content = serde_json::to_string(&message.content).unwrap();
    let related_players = message.parties.related_players();
    let related_players = related_players
        .into_iter()
        .map(|pi| {
            (
                RelatedPlayer {
                    message_id: 0,
                    uuid: pi.uuid.to_string(),
                },
                pi.alias,
            )
        })
        .collect();
    let msg = Message {
        id: 0,
        time: message.time,
        party_type: party_type.into(),
        parties,
        content,
    };
    (msg, related_players)
}

fn parse_simple_message(row: AnyRow, db_type: &DbType) -> Result<simple::Message, sqlx::Error> {
    Ok(simple::Message {
        id: row.try_get("id")?,
        // TODO: Handle this error
        time: util::str_to_date(row.try_get("timeS")?, db_type).unwrap(),
        parties: row.try_get("parties")?,
        content: row.try_get("content")?,
    })
}
