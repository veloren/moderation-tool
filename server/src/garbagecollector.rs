use core::{future::Future, time::Duration};
use db::{actions, Db};
use std::sync::Arc;

/// We promised the community to hold it maximum for 1 month: https://discord.com/channels/449602562165833758/471990111710478336/1171433507080372254
/// To comply with that we set the Retention_period to 28 days
const RETENTION_PERIOD: Duration = Duration::from_secs(86400 * 28);

const DELETE_INTERVAL: Duration = Duration::from_secs(3600);

pub async fn run<F>(db: Arc<Db>, shutdown: F)
where
    F: Future<Output = ()>,
{
    tokio::select! {
        _ = shutdown => (),
        _ = async move {
            let db = db;
            loop {
                let keep_from = chrono::Utc::now() - (RETENTION_PERIOD - DELETE_INTERVAL);
                match actions::garbagecollector::delete_unrelevant_messages(keep_from, &db).await {
                    Ok(affected) => tracing::info!(?affected, ?keep_from, "Deleted old messages"),
                    Err(e) => tracing::error!(?keep_from, ?e, "could not delete old messages"),
                }
                tokio::time::sleep(DELETE_INTERVAL).await;
            }
        } => (),
    };
}
