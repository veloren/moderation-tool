use clap::Parser;
use core::sync::atomic::{AtomicUsize, Ordering};
use db::{actions, Db};
use std::{sync::Arc, time::Duration};
use tokio::sync::Notify;
use tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, EnvFilter};

mod api_ext;
mod clap_cli;
mod fetcher;
mod garbagecollector;
mod server;

fn main() {
    let args = clap_cli::Cli::parse();

    let logger = tracing_subscriber::fmt::layer();
    let filter = EnvFilter::default();
    let mut filter = filter.add_directive("hyper=INFO".parse().unwrap());
    match args.verbose {
        0 => filter = filter.add_directive("INFO".parse().unwrap()),
        1 => filter = filter.add_directive("DEBUG".parse().unwrap()),
        _ => filter = filter.add_directive("TRACE".parse().unwrap()),
    }
    let collector = tracing_subscriber::Registry::default()
        .with(logger)
        .with(filter);

    // Initialize tracing
    tracing::subscriber::set_global_default(collector).unwrap();
    tracing::info!("Veloren Moderation Server");

    let runtime = Arc::new(
        tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .thread_name_fn(|| {
                static ATOMIC_ID: AtomicUsize = AtomicUsize::new(0);
                let id = ATOMIC_ID.fetch_add(1, Ordering::SeqCst);
                format!("tokio-server-{}", id)
            })
            .build()
            .unwrap(),
    );

    let registry = Arc::new(prometheus::Registry::new());
    let web_shutdown = Arc::new(Notify::new());
    let web_shutdown_clone = Arc::clone(&web_shutdown);
    let web_shutdown_clone2 = Arc::clone(&web_shutdown);
    let web_shutdown_clone3 = Arc::clone(&web_shutdown);

    let db = runtime
        .block_on(Db::new(args.database))
        .expect("Failed to initialize database");
    let latest_timestamp = runtime
        .block_on(actions::message::get_latest_timestamp(&db))
        .expect("Could not get latest message timestamp");

    let db = Arc::new(db);
    let db2 = Arc::clone(&db);
    let db3 = Arc::clone(&db);

    let fetcher = gameserver_api::run(
        args.chat_token,
        args.gameserver,
        latest_timestamp,
        Duration::from_secs(10),
        fetcher::store_messages(db),
        web_shutdown_clone.notified(),
    );
    let gc = garbagecollector::run(db2, web_shutdown_clone2.notified());
    let server = server::run(
        registry,
        args.listen_addr,
        db3,
        Some(args.moderation_endpoint_token),
        web_shutdown_clone3.notified(),
    );

    runtime.block_on(async move {
        tokio::select! {
            _ = fetcher => (),
            _ = gc => (),
            e = server => {
                tracing::error!(?e, "Server crashed, stopping moderation tool");
            }
        }
    });
}
