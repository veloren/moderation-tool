pub mod actions;
pub mod error;
#[allow(dead_code)] pub mod models;
pub(crate) mod util;

pub use models::simple;

use sqlx::{Executor, Pool};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum DbType {
    Sqlite,
    Postgresql,
}

pub struct Db {
    pub(crate) pool: Pool<sqlx::any::Any>,
    pub(crate) t: DbType,
}

impl Db {
    pub async fn new(conection_args: Option<String>) -> Result<Self, error::DatabaseInitError> {
        sqlx::any::install_default_drivers();
        let conection_args = conection_args
            .as_deref()
            .unwrap_or("sqlite://moderation-db.sqlite?mode=rwc");

        let t = if conection_args.starts_with("sqlite:") {
            DbType::Sqlite
        } else {
            DbType::Postgresql
        };

        let pool = sqlx::any::AnyPoolOptions::new()
            .max_connections(match t {
                DbType::Sqlite => 1,
                DbType::Postgresql => 10,
            })
            .connect(conection_args)
            .await
            .expect("cannot connect to database, unable to start moderation-tool");

        let x = pool.options();
        tracing::info!(?x, "dasda");
        if t == DbType::Sqlite {
            pool.execute("PRAGMA journal_mode = WAL;").await.unwrap();
            pool.execute("PRAGMA busy_timeout = 250;").await.unwrap();
        }

        sqlx::migrate!().run(&pool).await?;

        Ok(Self { pool, t })
    }
}
