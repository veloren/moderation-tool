use argon2::{
    password_hash::{rand_core::OsRng, PasswordHasher, SaltString},
    Argon2,
};
use axum::{
    extract::{Path, Query, State},
    middleware::Next,
    response::Response,
    routing::{delete, get, post, put},
    Json, Router,
};
use db::{actions, Db};
use hyper::{Request, StatusCode};
use serde::Deserialize;
use std::sync::Arc;

#[derive(Clone)]
struct ModeratorsToken {
    secret_token: Option<String>,
}

pub fn router(db: Arc<Db>, secret_token: Option<String>) -> Router {
    let token = ModeratorsToken { secret_token };
    Router::new()
        .route("/mod", post(add_mod))
        .route("/mod", get(list_mod))
        .route("/mod/:name", delete(delete_mod))
        .route("/mod/:name/active", put(set_active_mod))
        .route("/mod/:name/password", put(set_password_mod))
        .layer(axum::middleware::from_fn_with_state(token, validate_secret))
        .with_state(db)
}

async fn validate_secret<B>(
    State(token): State<ModeratorsToken>,
    req: Request<B>,
    next: Next<B>,
) -> Result<Response, StatusCode> {
    // check if this endpoint is disabled
    let secret_token = token.secret_token.ok_or(StatusCode::METHOD_NOT_ALLOWED)?;

    pub const X_SECRET_TOKEN: &str = "X-Secret-Token";
    let session_cookie = req
        .headers()
        .get(X_SECRET_TOKEN)
        .ok_or(StatusCode::UNAUTHORIZED)?;

    if session_cookie.as_bytes() != secret_token.as_bytes() {
        return Err(StatusCode::UNAUTHORIZED);
    }

    Ok(next.run(req).await)
}

#[derive(Debug, Deserialize)]
pub struct ModeratorParams {
    pub username: String,
    pub password: String,
}

fn hash(pw: &str) -> Result<String, StatusCode> {
    let salt = SaltString::generate(&mut OsRng);
    let argon2 = Argon2::default();
    let password_hash = argon2
        .hash_password(pw.as_bytes(), &salt)
        .map_err(|_| StatusCode::BAD_REQUEST)?
        .to_string();
    Ok(password_hash)
}

async fn list_mod(State(db): State<Arc<Db>>) -> Result<Json<Vec<(String, bool)>>, StatusCode> {
    let list = actions::moderator::list(&db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(Json(
        list.into_iter()
            .map(|moderator| (moderator.name, moderator.active))
            .collect::<Vec<_>>(),
    ))
}

async fn add_mod(
    State(db): State<Arc<Db>>,
    Query(params): Query<ModeratorParams>,
) -> Result<(), StatusCode> {
    let password_hash = hash(&params.password)?;

    actions::moderator::add(&params.username, &password_hash, &db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}

async fn delete_mod(State(db): State<Arc<Db>>, Path(name): Path<String>) -> Result<(), StatusCode> {
    actions::moderator::remove(&name, &db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}

async fn set_active_mod(
    State(db): State<Arc<Db>>,
    Path(name): Path<String>,
    Query(active): Query<bool>,
) -> Result<(), StatusCode> {
    actions::moderator::set_active(&name, active, &db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}

async fn set_password_mod(
    State(db): State<Arc<Db>>,
    Path(name): Path<String>,
    Query(password): Query<String>,
) -> Result<(), StatusCode> {
    let password_hash = hash(&password)?;

    actions::moderator::set_password(&name, &password_hash, &db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}
