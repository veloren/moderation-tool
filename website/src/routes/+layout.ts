import { get } from 'svelte/store'
import { authToken } from '../stores.js';

export const load: PageLoad = async ({ fetch, url }) => {
    const token = get(authToken);
    if (token == null || token == "") {
        return {
            username: null,
        };
    } else {
        console.log("--", token)
        let raw_token = token.replace('v4.public.', '')
        let decoded_token = null
        // I havent found a better way to detect the end of the encoded JSON :/
        while (raw_token.length > 1) {
            raw_token = raw_token.substring(0, raw_token.length - 1)
            try {
                decoded_token = atob(raw_token)
                if (decoded_token.endsWith('}')) {
                    break;
                }
            } catch (e) {
            }
        }
        if (decoded_token == null) {
            return {
                username: null,
            };
        }

        const obj = JSON.parse(decoded_token);
        return {
            username: obj.username,
        };
    }
};