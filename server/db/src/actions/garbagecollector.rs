use crate::Db;
use chrono::{DateTime, Utc};
use tracing::instrument;

#[instrument(skip(db), err)]
pub async fn delete_unrelevant_messages(
    older_than: DateTime<Utc>,
    db: &Db,
) -> Result<u64, sqlx::Error> {
    //TODO, for now all msg are unrelevant
    let rows_affected = sqlx::query("DELETE FROM message WHERE time < ?")
        .bind(older_than.to_string())
        .execute(&db.pool)
        .await?
        .rows_affected();

    Ok(rows_affected)
}
