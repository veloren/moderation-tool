//! Extentions to the API
use api::v1::{ext::ChatPartiesExt, ChatMessage, Content, PlayerInfo};

pub fn prettify_content(c: &Content) -> String {
    match c {
        Content::Plain(c) => c.to_string(),
        Content::Localized { key, seed: _, args } => {
            format!("#L:{key} - {:?}", args)
        },
    }
}

pub fn prettify_playerinfo(pi: &PlayerInfo) -> String { format!("{}({})", pi.alias, pi.uuid) }

pub fn prettify_msg(msg: &ChatMessage, ignore_day: bool) -> String {
    let party_name = msg.parties.party_name();
    let time = if ignore_day {
        msg.time.format("%H:%M:%S").to_string()
    } else {
        msg.time.format("%Y-%m-%d %H:%M:%S").to_string()
    };

    let parties = msg.parties.clone().related_players();

    let party = parties
        .iter()
        .map(prettify_playerinfo)
        .collect::<Vec<_>>()
        .join(" ");
    let content = prettify_content(&msg.content);
    let content = match content.as_str() {
        "" => content,
        _ => format!("\"{content}\""),
    };

    format!("[{time}] {party_name}: {content} - {party}")
}
