use clap::Parser;
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[arg(short, action = clap::ArgAction::Count)]
    pub verbose: u8,

    #[arg(long, env="LISTEN_ADDR", default_value_t = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, 8080)))]
    /// to be specified if you want to change the port or only listen on
    /// 127.0.0.1 instead of 0.0.0.0
    pub listen_addr: SocketAddr,

    #[arg(long, env = "GAMESERVER")]
    /// Address to the Gameserver Web Api, e.g. https://server.veloren.net:14005
    pub gameserver: String,

    #[arg(long, env = "CHAT_TOKEN")]
    /// Secret_token for Chat API access of the gameserver
    pub chat_token: String,

    #[arg(long, env = "MODERATION_ENDPOINT_TOKEN")]
    /// Secret_token for Moderation API admin access. Keep it private or
    /// otherwise people might create moderators at will
    pub moderation_endpoint_token: String,

    #[arg(long, env = "DATABASE")]
    /// database connection info, if left empty we will create a sqlite database
    /// locally
    pub database: Option<String>,
}
