use argon2::{
    password_hash::{PasswordHash, PasswordVerifier},
    Argon2,
};
use axum::{
    async_trait,
    extract::{FromRequestParts, Path, Query, State},
    headers::{authorization::Bearer, Authorization},
    http::request::Parts,
    routing::{get, put},
    RequestPartsExt, Router, TypedHeader,
};
use db::{actions, Db};
use hyper::StatusCode;
use serde::Deserialize;
use std::sync::Arc;

use axum::extract::FromRef;
use core::convert::TryFrom;
use pasetors::{
    claims::{Claims, ClaimsValidationRules},
    keys::{AsymmetricKeyPair, Generate},
    public,
    token::UntrustedToken,
    version4::V4,
    Public,
};
use std::time::Duration;

pub struct Tokens {
    key_pair: AsymmetricKeyPair<V4>,
    validation_rules: ClaimsValidationRules,
}

#[derive(Clone)]
pub(crate) struct AuthState {
    pub db: Arc<Db>,
    pub tokens: Arc<Tokens>,
}

impl FromRef<AuthState> for Arc<Db> {
    fn from_ref(input: &AuthState) -> Self { input.db.clone() }
}

impl FromRef<AuthState> for Arc<Tokens> {
    fn from_ref(input: &AuthState) -> Self { input.tokens.clone() }
}

impl Tokens {
    pub fn new() -> Result<Self, pasetors::errors::Error> {
        let key_pair = AsymmetricKeyPair::<V4>::generate()?;
        let mut validation_rules = ClaimsValidationRules::new();
        validation_rules.validate_issuer_with("mod.veloren.net");
        validation_rules.validate_subject_with("veloren-moderation-tool");
        Ok(Self {
            key_pair,
            validation_rules,
        })
    }

    pub fn gen(&self, username: &str) -> Result<String, pasetors::errors::Error> {
        let mut claims = Claims::new_expires_in(&Duration::from_secs(3600 * 24))?;
        claims.issuer("mod.veloren.net")?;
        claims.subject("veloren-moderation-tool")?;
        claims.add_additional("username", username)?;
        let pub_token = public::sign(&self.key_pair.secret, &claims, None, None)?;
        Ok(pub_token)
    }

    pub fn valid(&self, token: &str) -> Result<String, pasetors::errors::Error> {
        let untrusted_token = UntrustedToken::<Public, V4>::try_from(token)?;
        let c = public::verify(
            &self.key_pair.public,
            &untrusted_token,
            &self.validation_rules,
            None,
            None,
        )?;
        let username = c
            .payload_claims()
            .ok_or(pasetors::errors::Error::InvalidClaim)?
            .get_claim("username")
            .ok_or(pasetors::errors::Error::InvalidClaim)?
            .to_string();
        Ok(username)
    }
}

pub fn router(db: Arc<Db>, tokens: Arc<Tokens>) -> Router {
    let state = AuthState { db, tokens };

    let cors = tower_http::cors::CorsLayer::new()
        // allow `GET` and `POST` when accessing the resource
        .allow_methods([hyper::Method::GET, hyper::Method::PUT])
        // allow requests from any origin
        .allow_origin(tower_http::cors::Any);

    Router::new()
        .route("/:username/token", get(token))
        .route("/:username/password", put(set_password))
        .layer(cors)
        .with_state(state)
}

#[derive(Debug, Deserialize)]
struct PW {
    password: String,
}

/// complete history of all messages in a time frame
async fn token(
    State(state): State<AuthState>,
    Path(username): Path<String>,
    Query(params): Query<PW>,
) -> Result<String, StatusCode> {
    let db_pass = actions::moderator::get_password(&username, &state.db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
        .ok_or(StatusCode::UNAUTHORIZED)?;

    let parsed_hash = PasswordHash::new(&db_pass).map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Argon2::default()
        .verify_password(params.password.as_bytes(), &parsed_hash)
        .map_err(|_| StatusCode::UNAUTHORIZED)?;

    let token = state
        .tokens
        .gen(&username)
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    Ok(token)
}

#[derive(Debug, Deserialize)]
struct PW2 {
    old_password: String,
    new_password: String,
}

async fn set_password(
    State(state): State<AuthState>,
    Path(username): Path<String>,
    Query(params): Query<PW2>,
) -> Result<(), StatusCode> {
    let db_pass = actions::moderator::get_password(&username, &state.db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
        .ok_or(StatusCode::UNAUTHORIZED)?;
    let parsed_hash = PasswordHash::new(&db_pass).map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Argon2::default()
        .verify_password(params.old_password.as_bytes(), &parsed_hash)
        .map_err(|_| StatusCode::UNAUTHORIZED)?;
    actions::moderator::set_password(&username, &params.new_password, &state.db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}

#[derive(Debug, Deserialize)]
pub(crate) struct Moderator {
    pub name: String,
}

#[async_trait]
impl<S> FromRequestParts<S> for Moderator
where
    S: Sync,
    Arc<Tokens>: FromRef<S>,
{
    type Rejection = StatusCode;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        // Extract the token from the authorization header
        let TypedHeader(Authorization(bearer)) = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await
            .map_err(|_| StatusCode::UNAUTHORIZED)?;
        let tokens = Arc::<Tokens>::from_ref(state);
        let name = tokens
            .valid(bearer.token())
            .map_err(|_| StatusCode::UNAUTHORIZED)?;

        Ok(Self { name })
    }
}
