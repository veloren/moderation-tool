import { writable } from 'svelte/store';
import type { Writable } from 'svelte/store';
import { browser } from '$app/environment';

const storedAuthToken = browser? localStorage.getItem("authToken") : null;
export const authToken: Writable<null | string> = writable(storedAuthToken);
if (browser) {
    authToken.subscribe(value => {
        localStorage.setItem("authToken", value || "");
    });
}
