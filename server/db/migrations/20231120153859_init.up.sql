-- basic functionality to store messages
CREATE TABLE message (
    id INTEGER PRIMARY KEY,
    time DATETIME NOT NULL,
    party_type INTEGER NOT NULL,
    parties STRING NOT NULL,
    content STRING NOT NULL
);

CREATE TABLE related_player (
    message_id INTEGER NOT NULL,
    uuid STRING NOT NULL,
    FOREIGN KEY(message_id) REFERENCES message(id)
);

CREATE TABLE alias (
    uuid STRING NOT NULL,
    alias STRING NOT NULL,
    PRIMARY KEY(uuid, alias)
);

CREATE TABLE moderator (
    name STRING PRIMARY KEY,
    password_hashed STRING NOT NULL,
    active BOOLEAN NOT NULL
);

CREATE INDEX related_player_message_idx ON related_player(message_id);
CREATE INDEX related_player_uuid_idx ON related_player(uuid);
CREATE INDEX alias_uuid_idx ON alias(uuid);
