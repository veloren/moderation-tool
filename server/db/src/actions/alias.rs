use crate::Db;
use sqlx::Row;
use tracing::instrument;

#[instrument(skip(db), err)]
pub async fn aliases_of(uuid: &str, db: &Db) -> Result<Vec<String>, sqlx::Error> {
    let rows = sqlx::query("SELECT alias FROM alias WHERE uuid = ?")
        .bind(uuid)
        .fetch_all(&db.pool)
        .await?;

    rows.into_iter()
        .map(|row| row.try_get("alias"))
        .collect::<Result<Vec<_>, _>>()
}

#[instrument(skip(db), err)]
pub async fn uuid_of(alias: &str, db: &Db) -> Result<Vec<String>, sqlx::Error> {
    let rows = sqlx::query("SELECT uuid FROM alias WHERE alias = ?")
        .bind(alias)
        .fetch_all(&db.pool)
        .await?;

    rows.into_iter()
        .map(|row| row.try_get("uuid"))
        .collect::<Result<Vec<_>, _>>()
}
