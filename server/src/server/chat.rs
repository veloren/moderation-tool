use crate::server::auth::{AuthState, Moderator, Tokens};
use axum::{
    extract::{Path, Query, State},
    http,
    response::IntoResponse,
    routing::get,
    Json, Router,
};
use chrono::{DateTime, Utc};
use db::{actions, models, Db};
use hyper::StatusCode;
use serde::{Deserialize, Serialize};
use std::sync::Arc;

pub fn router(db: Arc<Db>, tokens: Arc<Tokens>) -> Router {
    let state = AuthState { db, tokens };

    let cors = tower_http::cors::CorsLayer::new()
        // allow `GET` and `POST` when accessing the resource
        .allow_methods([hyper::Method::GET])
        .allow_headers([http::header::AUTHORIZATION])
        // allow requests from any origin
        .allow_origin(tower_http::cors::Any);

    Router::new()
        .route("/history", get(history))
        .route("/player", get(player))
        .route("/alias/:uuid", get(alias))
        .route("/uuid/:alias", get(uuid))
        .layer(cors)
        .with_state(state)
}

#[derive(Debug, Deserialize)]
struct TimeParams {
    /// rfc3339
    from: String,
    /// rfc3339
    to: String,
}

#[derive(Debug, Serialize)]
pub struct Message {
    pub id: i64,
    pub time: DateTime<Utc>,
    pub parties: String,
    pub content: String,
}

/// complete history of all messages in a time frame
async fn history(
    State(db): State<Arc<Db>>,
    Query(params): Query<TimeParams>,
    _moderator: Moderator, // needed for user verification
) -> Result<Json<Vec<models::simple::Message>>, StatusCode> {
    let from = DateTime::parse_from_rfc3339(&params.from).map_err(|_| StatusCode::BAD_REQUEST)?;
    let to = DateTime::parse_from_rfc3339(&params.to).map_err(|_| StatusCode::BAD_REQUEST)?;
    let msg = actions::message::history(from.into(), to.into(), &db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(Json(msg))
}

/// messages related to a list of players
async fn player(
    State(db): State<Arc<Db>>,
    Query(params): Query<TimeParams>,
    moderator: Moderator, // needed for user verification
    Json(players): Json<Vec<String>>,
) -> Result<impl IntoResponse, StatusCode> {
    let from = DateTime::parse_from_rfc3339(&params.from).map_err(|_| StatusCode::BAD_REQUEST)?;
    let to = DateTime::parse_from_rfc3339(&params.to).map_err(|_| StatusCode::BAD_REQUEST)?;
    tracing::info!(?moderator.name, "player request by");
    let msg = actions::message::player_history(from.into(), to.into(), players, &db)
        .await
        .map_err(|e| {
            tracing::warn!(?e, "SQL ERROR");
            StatusCode::INTERNAL_SERVER_ERROR
        })?;

    Ok(Json(msg))
}

/// converting UUID to Alias
async fn alias(
    State(db): State<Arc<Db>>,
    Path(uuid): Path<String>,
    _moderator: Moderator, // needed for user verification
) -> Result<Json<Vec<String>>, StatusCode> {
    let aliases = actions::alias::aliases_of(&uuid, &db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    Ok(Json(aliases))
}

/// converting Alias to UUID
async fn uuid(
    State(db): State<Arc<Db>>,
    Path(alias): Path<String>,
    _moderator: Moderator, // needed for user verification
) -> Result<Json<Vec<String>>, StatusCode> {
    let uuids = actions::alias::uuid_of(&alias, &db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    Ok(Json(uuids))
}
