use crate::{simple, Db};
use sqlx::Row;
use tracing::instrument;

#[instrument(skip(db), err)]
pub async fn list(db: &Db) -> Result<Vec<simple::Moderator>, sqlx::Error> {
    sqlx::query_as::<_, simple::Moderator>("SELECT name, active FROM moderator")
        .fetch_all(&db.pool)
        .await
}

#[instrument(skip(db), err)]
pub async fn get_password(username: &str, db: &Db) -> Result<Option<String>, sqlx::Error> {
    let row = sqlx::query("SELECT password_hashed FROM moderator WHERE name = ?")
        .bind(username)
        .fetch_optional(&db.pool)
        .await?;

    row.map(|row| row.try_get("password_hashed")).transpose()
}

#[instrument(skip(db, password_hash), err)]
pub async fn add(username: &str, password_hash: &str, db: &Db) -> Result<(), sqlx::Error> {
    sqlx::query("INSERT INTO moderator (name, password_hashed, active) VALUES (?, ?, True)")
        .bind(username)
        .bind(password_hash)
        .execute(&db.pool)
        .await?;

    Ok(())
}

#[instrument(skip(db), err)]
pub async fn remove(username: &str, db: &Db) -> Result<(), sqlx::Error> {
    sqlx::query("DELETE FROM moderator WHERE name = ?")
        .bind(username)
        .execute(&db.pool)
        .await?;

    Ok(())
}

#[instrument(skip(db), err)]
pub async fn set_active(username: &str, active: bool, db: &Db) -> Result<(), sqlx::Error> {
    sqlx::query("UPDATE moderater SET active = ? WHERE name = ?")
        .bind(active)
        .bind(username)
        .execute(&db.pool)
        .await?;

    Ok(())
}

#[instrument(skip(db, password_hash), err)]
pub async fn set_password(username: &str, password_hash: &str, db: &Db) -> Result<(), sqlx::Error> {
    sqlx::query("UPDATE moderator SET password_hashed = ? WHERE name = ?")
        .bind(password_hash)
        .bind(username)
        .execute(&db.pool)
        .await?;
    Ok(())
}
