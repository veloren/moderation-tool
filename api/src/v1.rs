use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub const X_SECRET_TOKEN: &str = "X-Secret-Token";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum LocalizationArg {
    Content(Content),
    Nat(u64),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Content {
    Plain(String),
    Localized {
        key: String,
        seed: u16,
        args: HashMap<String, LocalizationArg>,
    },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum KillType {
    Buff(String),
    Melee,
    Projectile,
    Explosion,
    Energy,
    Other,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PlayerInfo {
    pub uuid: uuid::Uuid,
    pub alias: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum KillSource {
    Player(PlayerInfo, KillType),
    NonPlayer(String, KillType),
    NonExistent(KillType),
    Environment(String),
    FallDamage,
    Suicide,
    Other,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
/// partially mapped to common::comp::ChatMsg
pub enum ChatParties {
    Online(PlayerInfo),
    Offline(PlayerInfo),
    CommandInfo(PlayerInfo),
    CommandError(PlayerInfo),
    Kill(KillSource, PlayerInfo),
    GroupMeta(Vec<PlayerInfo>),
    Group(PlayerInfo, Vec<PlayerInfo>),
    Tell(PlayerInfo, PlayerInfo),
    Say(PlayerInfo),
    FactionMeta(String),
    Faction(PlayerInfo, String),
    Region(PlayerInfo),
    World(PlayerInfo),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChatMessage {
    pub time: DateTime<Utc>,
    pub parties: ChatParties,
    pub content: Content,
}

pub mod ext {
    use super::{ChatParties, KillSource, PlayerInfo};

    pub trait ChatPartiesExt {
        fn party_type(&self) -> i32;

        /// Some messages are more private by nature than others, e.g. direct
        /// messages. Even though its clear they are monitored, they are
        /// not displayed by default in history but require a search via
        /// an affected user. This should avoid potential misuse by
        /// moderators
        fn potentially_sensible(&self) -> bool;

        fn party_name(&self) -> &'static str;

        fn related_players(self) -> Vec<PlayerInfo>;
    }

    impl ChatPartiesExt for ChatParties {
        fn party_type(&self) -> i32 {
            use ChatParties::*;

            match self {
                Online(_) => 0,
                Offline(_) => 1,
                CommandInfo(_) => 2,
                CommandError(_) => 3,
                Kill(_, _) => 4,
                GroupMeta(_) => 5,
                Group(_, _) => 6,
                Tell(_, _) => 7,
                Say(_) => 8,
                FactionMeta(_) => 9,
                Faction(_, _) => 10,
                Region(_) => 11,
                World(_) => 12,
            }
        }

        fn potentially_sensible(&self) -> bool {
            use ChatParties::*;

            match self {
                Online(_)
                | Offline(_)
                | CommandInfo(_)
                | CommandError(_)
                | Kill(_, _)
                | GroupMeta(_)
                | Say(_)
                | FactionMeta(_)
                | Faction(_, _)
                | Region(_)
                | World(_) => false,
                Group(_, _) => true,
                Tell(_, _) => true,
            }
        }

        fn party_name(&self) -> &'static str {
            use ChatParties::*;

            match self {
                Online(_) => "Online",
                Offline(_) => "Offline",
                CommandInfo(_) => "CommandInfo",
                CommandError(_) => "CommandError",
                Kill(_, _) => "Kill",
                GroupMeta(_) => "GroupMeta",
                Say(_) => "Say",
                FactionMeta(_) => "FactionMeta",
                Faction(_, _) => "Faction",
                Region(_) => "Region",
                World(_) => "World",
                Group(_, _) => "Group",
                Tell(_, _) => "Tell",
            }
        }

        fn related_players(self) -> Vec<PlayerInfo> {
            use ChatParties::*;

            match self {
                Online(pi) => vec![pi],
                Offline(pi) => vec![pi],
                CommandInfo(pi) => vec![pi],
                CommandError(pi) => vec![pi],
                Kill(ks, pi) => match ks {
                    KillSource::Player(pi2, _) => vec![pi, pi2],
                    _ => vec![pi],
                },
                GroupMeta(pis) => pis,
                Group(pi, mut pis) => {
                    let mut res = vec![pi];
                    res.append(&mut pis);
                    res
                },
                Tell(pi1, pi2) => vec![pi1, pi2],
                Say(pi) => vec![pi],
                FactionMeta(_) => Vec::new(),
                Faction(pi, _) => vec![pi],
                Region(pi) => vec![pi],
                World(pi) => vec![pi],
            }
        }
    }
}
