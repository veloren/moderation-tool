use chrono::{DateTime, Utc};
use sqlx::FromRow;

#[derive(FromRow, Debug)]
pub(crate) struct Message {
    pub id: i64,
    pub time: DateTime<Utc>,
    pub party_type: i64,
    pub parties: String,
    pub content: String,
}

#[derive(FromRow, Debug)]
pub(crate) struct RelatedPlayer {
    pub message_id: i64,
    pub uuid: String,
}

#[derive(FromRow, Debug)]
pub(crate) struct Alias {
    pub uuid: String,
    pub alias: String,
}

#[derive(FromRow, Debug)]
pub(crate) struct Moderator {
    pub name: String,
    pub password_hashed: String,
    pub active: bool,
}

pub mod simple {
    use chrono::{DateTime, Utc};
    use serde::Serialize;
    use sqlx::FromRow;

    #[derive(Serialize)]
    pub struct Message {
        pub id: i64,
        pub time: DateTime<Utc>,
        pub parties: String,
        pub content: String,
    }

    #[derive(FromRow)]
    pub struct Moderator {
        pub name: String,
        pub active: bool,
    }
}
