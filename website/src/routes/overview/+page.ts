import type { PageLoad } from './$types';
import { get } from 'svelte/store'
import { authToken } from '../../stores.js';
import { parseMessages } from '../../message.js';
import { formatDate, formatTime, formatDateTime } from '../../chrono.js';
import { goto } from '$app/navigation';
import { browser } from '$app/environment';

export const load: PageLoad = async ({ fetch, url }) => {

    const token = get(authToken);
    if (token == null || token == "") {
        if (browser) {
            goto('/login');
        }
        return {
            status: 999,
            messages: [],
            from_date: "2024-02-01",
            from_time: "00:00:00",
            range: "21600",
        };
    } else {
        const now = new Date()
        const from = url.searchParams.get("from") || formatDateTime(new Date(now.getTime() - 21600 * 1000), true)
        const range = url.searchParams.get("range") || "21600"
        const from_date: Date = new Date(from)
        const range_millis = Number(range) * 1000
        const to_millis: Date = new Date(from_date.getTime() + range_millis)
        const to = to_millis.toISOString()
        const history_url = `https://modapi.veloren.net/chat/v1/history?from=${from}&to=${to}`
        const response = await fetch(history_url, {
            method: 'GET',
            headers: {
                "Authorization": "Bearer " + token
            }
        });
    
        let status = response.status
        console.log("Status: " + status)
        if (status == 200) {
            let text = await response.text();
            let messages = parseMessages(text)
            console.log("found " + messages.length + " messages")

            return {
                status,
                messages,
                from_date: formatDate(from_date),
                from_time: formatTime(from_date),
                range: range,
            };
        } else {
            if (browser) {
                goto('/login');
            }
            return {
                status,
                messages: [],
            };
        }

    

    }
};