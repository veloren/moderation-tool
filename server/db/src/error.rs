use thiserror::Error;

#[derive(Error, Debug)]
pub enum DatabaseInitError {
    #[error("Failed to apply database migrations: {0:?}")]
    MigrationFailed(#[from] sqlx::migrate::MigrateError),
}
