# Veloren Moderation Tool

This tool grabs messages from veloren and preprocesses them for moderation tasks.

## Server

### Run
```bash
cargo run -- --chat-token "secretpassword" --moderation-endpoint-token "admintoken" --gameserver http://localhost:14005 -vv
```

### API commands
```bash
export ADMIN_TOKEN="admintoken"
export PASSWORD="hunter2"
export USER_NAME="root"
export SERVER="localhost:8080"
# Create User
curl -H "X-Secret-Token: ${ADMIN_TOKEN}" -v -X POST "${SERVER}/moderators/v1/mod?username=${USER_NAME}&password=${PASSWORD}"

#Get Token User
export TOKEN=$(curl "${SERVER}/auth/v1/${USER_NAME}/token?password=${PASSWORD}" -v) && echo $TOKEN

#Fetch UUIDs of Aliases
curl -H "Authorization: Bearer ${TOKEN}" ${SERVER}/chat/v1/uuid/username -v

#Fetch all general Messages in 2023
curl  -H "Authorization: Bearer ${TOKEN}" "${SERVER}/chat/v1/history?from=2023-01-01T00:00:00Z&to=2023-12-31T23:59:59Z" -v

# Fetch all general Messages in 2023 of specific users
export UUIDS='["75715c9d-3d5e-47fd-1234-2ab3cd4ef567"]'
curl -H "Authorization: Bearer ${TOKEN}" -H "Content-Type: application/json" -d "${UUIDS}" "${SERVER}/chat/v1/player?from=2023-01-01T00:00:00Z&to=2023-12-31T23:59:59Z" -X GET -v
```

## Svelte Website

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
# start the server and open the app in a new browser tab
npm run dev -- --open
```
