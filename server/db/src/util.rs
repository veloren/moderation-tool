use crate::DbType;
use chrono::{DateTime, NaiveDateTime, Utc};

pub(crate) fn str_to_date(time: &str, db: &DbType) -> Option<DateTime<Utc>> {
    const SQLITE_FORMAT: &str = "%Y-%m-%d %H:%M:%S.%f %Z";

    match db {
        DbType::Sqlite => NaiveDateTime::parse_from_str(time, SQLITE_FORMAT)
            .map(|date| date.and_utc())
            .ok(),
        DbType::Postgresql => unimplemented!(),
    }
}
